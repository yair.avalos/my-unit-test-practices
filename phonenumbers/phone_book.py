# Init

class PhoneBook():
    """
    PhoneBook Class
    for Python Testing Course Purposes
    """
    def __init__(self):
        """
        Constructor for PhoneBook
        """
        self.numbers = {}

    def add(self, name, number):
        """
        add method to introduce a name and phone number
        """
        self.numbers[name] = number

    def lookup(self, name):
        """
        lookup method to retrieve a number for a given name
        """
        return self.numbers[name]

    def is_consistent(self):
        """
        is_consistent method to give a phone_book status
        """
        for name1, number1 in self.numbers.items():
            for name2, number2 in self.numbers.items():
                if name1 == name2:
                    continue
                if number1.startswith(number2):
                    return False
        return True
