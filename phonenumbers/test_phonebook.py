# Standard Libraries and Packages
import unittest

# Project Libraries and Packages
from phonenumbers.phone_book import PhoneBook


class PhoneBookTest(unittest.TestCase):
    """
     PhoneBookTest
     Created for Python Testing Course
    """

    # Fixture ordering
    # 1st setUp
    def setUp(self):
        """
         setUp from super() / parent
         This help to refactor the below methods
        """
        self.phone_book = PhoneBook()

    # 2nd Test Cases
    def test_lookup_by_name(self):
        """
         test_lookup_by_name test case
        """
        self.phone_book.add("Bob", "11223344")
        number = self.phone_book.lookup("Bob")
        self.assertEqual("11223344", number)

    def test_missing_name(self):
        """
         test_missing_name test case
        """
        with self.assertRaises(KeyError):
            self.phone_book.lookup("missing")

    # @unittest.skip("WIP")
    def test_empty_phone_book_is_consistent(self):
        """
         test_empty_phone_book_is_consistent test case
        """
        self.assertTrue(self.phone_book.is_consistent())

    def test_is_consistent_with_different_entries(self):
        """
         test_is_consistent
        """
        self.phone_book.add("Bob", "12345")
        self.phone_book.add("Anna", "67890")
        self.assertTrue(self.phone_book.is_consistent())

    def test_inconsistent_with_duplicated_entries(self):
        """
         test_inconsistent_with_duplicated_entries
        """
        self.phone_book.add("Bob", "12345")
        self.phone_book.add("Sue", "12345")
        self.assertFalse(self.phone_book.is_consistent())

    def test_inconsistent_with_duplicated_prefix(self):
        """
         test_inconsistent_with_duplicated_prefix
        """
        self.phone_book.add("Bob", "12345")
        self.phone_book.add("Sue", "123")
        self.assertFalse(self.phone_book.is_consistent())

    def test_phonebook_adds_names_and_numbers(self):
        """
         test_phonebook_adds_names_and_numbers
        """
        self.phone_book.add("Sue", "123344")
        self.assertIn("Sue", self.phone_book.get_names())
        self.assertIn("123344", self.phone_book.get_numbers())

    # 3rd tearDown (optionally)
    def tearDown(self):
        """
         tearDown from super() / parent
         This help to manage any open resource after testings
         for example to release a DB connection or a File IO
        """
        pass
