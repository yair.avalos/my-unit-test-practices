ipython==7.30.1
pytest==6.2.5
python-dotenv==0.19.2
python-environ==0.4.54
